import Image from 'next/image'
import styles from './page.module.css'
import Head from 'next/head'
import Link from 'next/link'

export default function Home() {
  return (
   <div>
    <Head>
        <title>My page title</title>
    </Head>
    This is Home <br />
    <Link href='/hai'>Coba Hai</Link> <br />
    <Link href='/form/register'>Register</Link> <br />
    <a href='/hai'>HAI HAI</a> <br />
    <a href='/product'>Product</a>
   </div>
  )
}
