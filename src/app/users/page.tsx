import Link from "next/link";
import { getList } from "./action";
import { UserDeleteModal } from "./delete/delete";
import UserDeleteBtn from "./delete/delete-btn";


export default async function UserListPage()
{
    let dataEl: JSX.Element[] = [];
    let data  = await getList();
    // console.log("Client", data);
    let deleteEl = null;
    

    data.forEach(d => {
        dataEl.push(<tr key={d.id}>
            <td>{d.id}</td>
            <td>{d.username}</td>
            <td>
                <UserDeleteBtn name={d.full_name?.toString()} id={d.id} />
            </td>
        </tr>);
    });

    return (
        <>
        <Link href="users/add">Add User</Link>
            <h3>User List</h3> <br />
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {dataEl}
                </tbody>
            </table>
            {deleteEl}
        </>
    )
}