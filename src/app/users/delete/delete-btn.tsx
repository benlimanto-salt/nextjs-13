"use client"
import { useState } from "react";
import { UserDeleteModal } from "./delete";

export default function UserDeleteBtn(prop: any) 
{
    const [show, setShow] = useState(false);

    if (show)
        return (<UserDeleteModal name={prop.name} userId={prop.id} cancel={() => {setShow(false); }} />);
    else 
        return (<button onClick={() => setShow(true)}>Delete</button>)
}