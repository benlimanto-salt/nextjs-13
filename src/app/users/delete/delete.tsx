'use client'
import React from "react";
import { useFormState } from "react-dom";
import { DeleteUserAction } from "../action";

const initialState = {
    message: ""
}

export function UserDeleteModal(prop: any)
{
    const [state, action] = useFormState(DeleteUserAction, initialState);

    return (
        <dialog open>
        <p>Want to delete {prop.username}?</p>
        <form action={action}>
            <input type="hidden" name="id" value={prop.userId} />
            <button>OK</button> <br />
            <button onClick={prop.cancel}>Cancel</button>
        </form>
        </dialog>
    )
}