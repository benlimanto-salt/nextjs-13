'use client'

import { useFormState } from "react-dom"
import { InsertUserAction } from "../action"
import Link from "next/link"
const initialState = {
    message: ""
}

export default function UserAddPage()
{
    const [state, action] = useFormState(InsertUserAction, initialState)
    console.log(state);

    if (state.message == "")
        return (
            <div>
                <form action={action}>
                    <input type="text" name="username" placeholder="Username" /> <br />
                    <input type="password" name="password" placeholder="Password" /> <br />
                    <input type="email" name="email" placeholder="email"/> <br />
                    <input type="text" name="full_name" placeholder="full name" /> <br />

                    <button>Add User</button>
                </form>
            </div>
        )
    else {
        setTimeout(() => {
            window.location.assign("/users");
        }, 1500);

        return (
            <div>
                <Link href="/users">Go Back To List</Link>
            </div>
        )
    }
}