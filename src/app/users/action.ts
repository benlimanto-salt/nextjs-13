"use server"

import { PrismaClient } from "@prisma/client";
import { genSaltSync, hashSync } from "bcrypt-ts";
import { revalidateTag } from "next/cache";

export async function getList()
{
    const prisma = new PrismaClient();
    let data  = await prisma.user.findMany();
    // console.log("Data", data);
    prisma.$disconnect();
    return data;
}

export async function InsertUser(data: any)
{
    const prisma = new PrismaClient();
    let pHash = "";

    const salt = genSaltSync(10);
    pHash = hashSync(data.password, salt);
    try {
        await prisma.user.create({
            data : {
                username: data.username,
                password: pHash,
                email: data.email,
                full_name: data.full_name
            }
        });
    }
    catch (e: any)
    {
        return false;
    }
    prisma.$disconnect();

    return true;
}

export async function DeleteUser(data: any)
{
    const prisma = new PrismaClient();
    
    console.log(data);

    await prisma.user.delete({
        where: {
            id : parseInt(data.id)
        }
    });

    prisma.$disconnect();

    return true;
}

export async function InsertUserAction(prevState: any, form: FormData)
{
    let res = await InsertUser({
        username: form.get("username")?.toString(),
        password: form.get("password")?.toString(),
        email: form.get("email")?.toString(),
        full_name: form.get("full_name")?.toString(),
    });

    return {
        message: "Success"
    };
}

export async function DeleteUserAction(prevState: any, form: FormData)
{
    let res = await DeleteUser({
        id: form.get("id")
    });

    revalidateTag("UserListPage");
    
    return {
        message: "Success"
    }
}