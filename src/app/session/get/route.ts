import { getIronSession } from "iron-session";


export async function GET(req: Request, res: Response)
{
    const session = await getIronSession(req, res, { cookieName: "iron-next", password: "1234567891011121314151617181920212223242526272829303132" });
    let u = (session as any).username
    return Response.json({
        username: u,
        data: session
    });
}