"use client"
import { useFormState } from "react-dom";
import { FormSessionAction } from "./action";
import { useState } from "react";

interface SessionInitStateI
{
    status?: boolean|null
    message: string|null
}

const initState: SessionInitStateI = {
    status: null,
    message: null
}

export default function SessionPage()
{
    // State, in fn/polygot...
    const [username, setUsername] = useState(null);

    const getData = () => {
        fetch("/session/get")
            .then(d => d.json())
            .then(d => {
                setUsername(d.username);
            });
    }

    const resetFromState = () => {
        state.status = null;
        state.message = ""
        console.log(state);
    }

    const [state, action] = useFormState(FormSessionAction, initState);
    console.log("Data : ", state);
    if (state.status == null) {
        return (
            <div>
                <form action={action}>
                <input type="text" name="username" />
                <button>Set Value</button>
                </form>
            </div>
        );
    }
    else {
        return (
            <div>   
                Value is : {username} <br />
                <button onClick={getData}>Pull Data</button>
                <button onClick={resetFromState}>Back</button>
            </div>
        );
    }

}