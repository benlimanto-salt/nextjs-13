'use client'
import { useFormState, useFormStatus } from "react-dom";
import { Register } from "./action";

const initialState = {
    message: "",
}

export default async function RegisterPage()
{
    const { pending } = useFormStatus();
    const [state, formAction] = useFormState(Register, initialState);

    return (
        <div>
            <form action={formAction}>
                <input type="text" name="username" />
                <button aria-disabled={pending}>Add</button>
            </form>
            <p>{state.message}</p>
        </div>
    )
}