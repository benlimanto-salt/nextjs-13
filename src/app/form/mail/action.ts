"use server"

import { Mailer } from "@/lib/mailer"

export async function SendMailForm(prevState: any, form: FormData)
{
    let mail = Mailer();
    let status: any;
    // console.log(form);

    let response = await fetch("http://localhost:3000");
    let bodyHtml = await response.text();

    try {
        status = await mail.sendMail({
            from: "ben@salt.id",
            to: form.get("email")?.toString(),
            text: form.get("message")?.toString(),
            html: bodyHtml
        });
    }
    catch(e: any)
    {
        return {
            status: false,
            message: "Failed to send email " + (e as Error).message
        }
    }

    if (status.rejected.length > 0)
    {
        return {
            status: false,
            message: "Failed to send email"
        }
    }

    return {
        status: true,
        message: "Success send mail"
    }
}