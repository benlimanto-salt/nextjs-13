"use client"

import { useFormState } from "react-dom";
import { SendMailForm } from "./action";

interface SendMailFormI
{
    status: null|boolean,
    message: null|string
}

const initialState: SendMailFormI = {
    status: null,
    message: ""
}

export default function MailPage()
{
    const [state, action] = useFormState(SendMailForm, initialState);

    return (
        <div>
            <form action={action}>
                <label>Name</label> <br />
                <input type="text" name="name"/>
                <hr />
                
                <label>Email</label> <br />
                <input type="email" name="email"/>
                <hr />

                <label>Message</label> <br />
                <textarea name="message" cols={30} rows={10}></textarea>
                <hr />

                <button>Send</button>
            </form>

            <p>
                {state.message}
            </p>
        </div>
    ); 
}