import { PrismaClient } from "@prisma/client";

export async function POST(req: Request) {
    const prisma = new PrismaClient();
    let form = await req.formData();

    const data = await prisma.product.create({
        data: {
            name: form.get("name")?.toString()
        }
    });
   
    return Response.redirect(process.env.NEXT_URL+"/product");
}