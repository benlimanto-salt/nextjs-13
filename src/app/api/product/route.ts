import { PrismaClient } from "@prisma/client";

export async function GET() {
    const prisma = new PrismaClient();

    const data = await prisma.product.findMany();
   
    return Response.json({ data })
}