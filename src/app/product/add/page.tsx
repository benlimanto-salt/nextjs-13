import { Metadata } from "next";

export const metadata: Metadata = {
    title: 'Add Product',
}

export default function AddPage()
{
    return (
        <form method="post" action="/api/product/add">
            Name : <input type="text" name="name"/>
            <button>Submit</button>
        </form>
    )
}