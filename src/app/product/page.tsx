import { PrismaClient } from '@prisma/client';
import { Metadata } from 'next';
export const metadata: Metadata = {
    title: 'List Product',
}

export default async function Product() {
    const prisma = new PrismaClient();

    const data = await prisma.product.findMany();

    let productArr: JSX.Element[] = [];

    data.forEach(d => {
        productArr.push(<div key={d.id}>
            {d.id}
            <br />
            {d.name}
        </div>);
    });

    return (
    <div>
        <a href='/'>Home</a> <br />
        This is Product <br />
        {productArr}
    </div>
    )
}
