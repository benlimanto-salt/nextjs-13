export async function GET(req: Request) {
    let form =  req.body;

    const url = new URL(req.url);
    console.log(url.searchParams);
    let res = new Response(url.searchParams.toString());
    res.headers.set("content-type", "text/html");
    return res;
}