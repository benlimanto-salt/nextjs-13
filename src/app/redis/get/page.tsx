import { redis } from "@/lib/redis";
import { Metadata } from "next";
import Link from "next/link";

export const metadata: Metadata = {
    title: "get the value"
}

export default async function RedisGetPage()
{
    const r = await redis();
    // @see https://www.npmjs.com/package/redis#usage
    let val = await r.get("key"); 
    
    await r.disconnect();

    return (
        <div>
            value : {val}
            <a href='/redis'>Back to set</a>
        </div>
    )
}