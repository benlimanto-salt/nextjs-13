import { redis } from "@/lib/redis"
import { Metadata } from "next";
import Link from "next/link";

export const metadata: Metadata = {
    title: "Set the value"
}

export default async function RedisPage()
{
    let expired = 2;
    const r = await redis();
    // @see https://www.npmjs.com/package/redis#usage
    await r.set('key', 'value', {
        EX: expired //@see https://redis.io/commands/set/#options
    });

    await r.disconnect();

    return (
        <div>
            Expired in {expired}
            <a href='/redis/get'>See the Value</a>
        </div>
    )
}