import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'
 
// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  // @see https://nextjs.org/docs/app/building-your-application/routing/middleware#conditional-statements
  console.log("masuk redirect : ", request.nextUrl.pathname);
  return NextResponse.redirect(new URL('/hai', request.url))
}
 
// See "Matching Paths" below to learn more
export const config = {
  matcher: '/session/:path*',
}