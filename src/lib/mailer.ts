import * as nodemailer from 'nodemailer'

export function Mailer()
{
    const mail = nodemailer.createTransport({
        host: (process.env.MAIL_HOST as any),
        port: (process.env.MAIL_PORT as any),
        secure: false,
        auth: {
          user: (process.env.MAIL_USER as any),
          pass: (process.env.MAIL_PASS as any)
        }
      });

    return mail;
}