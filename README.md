This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Prisma DB

To create or pull the structure from the DB :
```bash
npx prisma db pull --schema src/prisma/schema.prisma
```

Then to make the prisma have it's autocomplete (in .prisma), then 

```bash
npx prisma db generate --schema src/prisma/schema.prisma
```

## Redis 

To use redis, well.. just install redis client

```bash
npm install redis
```

## Mailer
Using nodemailer, need some package 

```
npm i nodemailer
npm i @types/nodemailer --save-dev
```